# Pantropy

[![Coverage Status](https://coveralls.io/repos/github/julienlevasseur/Pantropy/badge.svg?branch=master)](https://coveralls.io/github/julienlevasseur/Pantropy?branch=master)
[![Go Report Card](https://goreportcard.com/badge/github.com/julienlevasseur/Pantropy)](https://goreportcard.com/badge/github.com/julienlevasseur/Pantropy)

Pantropy meant to be a full set of tools to manage application workflow (from dev to prod).

## UI Mockup

![UI Mockup](pantropy_ui_mockup_with_details.png)

## Authors

- [Julien Levasseur](https://github.com/julienlevasseur)
